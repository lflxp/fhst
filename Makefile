PROJECT=tmp

pp: clean install
	fhst startproject $(PROJECT)
	cd tmp && fhst startapp test && go mod tidy && showme watch

all: clean install 
	fhst startproject $(PROJECT)
	cd $(PROJECT) && fhst startapp k1 k2 k3 k4 K5 K6 --jwt && go mod tidy
	cd $(PROJECT) && fhst tool admin && fhst tool meilisearch && make all

install: 
	go install

init:
	sh ./init.sh

stringer:
	go get golang.org/x/tools/cmd/stringer

test:
	go test ./...

clean:
	rm -rf $(PROJECT)