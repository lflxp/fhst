package project

import "embed"

//go:embed project
var Project embed.FS

//go:embed middlewares
var Middlewares embed.FS

//go:embed tool
var Tool embed.FS

//go:embed app
var App embed.FS

//go:embed views
var Views embed.FS

//go:embed controller
var Controller embed.FS

//go:embed model
var Model embed.FS
