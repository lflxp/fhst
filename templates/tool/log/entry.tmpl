package log

import (
	"fmt"
	"github.com/sirupsen/logrus"
	"os"
	"sync"
)

type Entry struct {
	rawEntries []*logrus.Entry
	lockers    []*sync.RWMutex
}

func (entry *Entry) AddEntry(item *logrus.Entry, locker *sync.RWMutex) {
	entry.rawEntries = append(entry.rawEntries, item)
	entry.lockers = append(entry.lockers, locker)
}

func (entry *Entry) Log(level logrus.Level, args ...interface{}) {
	for i, en := range entry.rawEntries {
		entry.lockers[i].RLock()
		defer entry.lockers[i].RUnlock()

		en.Log(level, args...)
	}
}

func (entry *Entry) Logln(level logrus.Level, args ...interface{}) {
	for i, en := range entry.rawEntries {
		entry.lockers[i].RLock()
		defer entry.lockers[i].RUnlock()

		en.Logln(level, args...)
	}
}

func (entry *Entry) Logf(level logrus.Level, format string, args ...interface{}) {
	for i, en := range entry.rawEntries {
		entry.lockers[i].RLock()
		defer entry.lockers[i].RUnlock()

		en.Logf(level, format, args...)
	}
}

func (entry *Entry) Trace(args ...interface{}) {
	entry.Log(logrus.TraceLevel, args...)
}

func (entry *Entry) Debug(args ...interface{}) {
	entry.Log(logrus.DebugLevel, args...)
}

func (entry *Entry) Print(args ...interface{}) {
	entry.Info(args...)
}

func (entry *Entry) Info(args ...interface{}) {
	entry.Log(logrus.InfoLevel, args...)
}

func (entry *Entry) Warn(args ...interface{}) {
	entry.Log(logrus.WarnLevel, args...)
}

func (entry *Entry) Warning(args ...interface{}) {
	entry.Warn(args...)
}

func (entry *Entry) Error(args ...interface{}) {
	entry.Log(logrus.ErrorLevel, args...)
}

func (entry *Entry) Fatal(args ...interface{}) {
	entry.Log(logrus.FatalLevel, args...)
	os.Exit(1)

}

func (entry *Entry) Panic(args ...interface{}) {
	entry.Log(logrus.PanicLevel, args...)
	panic(fmt.Sprint(args...))

}

func (entry *Entry) Tracef(format string, args ...interface{}) {
	entry.Logf(logrus.TraceLevel, format, args...)

}

func (entry *Entry) Debugf(format string, args ...interface{}) {
	entry.Logf(logrus.DebugLevel, format, args...)

}

func (entry *Entry) Infof(format string, args ...interface{}) {
	entry.Logf(logrus.InfoLevel, format, args...)
}

func (entry *Entry) Printf(format string, args ...interface{}) {
	entry.Infof(format, args...)

}

func (entry *Entry) Warnf(format string, args ...interface{}) {
	entry.Logf(logrus.WarnLevel, format, args...)
}

func (entry *Entry) Warningf(format string, args ...interface{}) {
	entry.Warnf(format, args...)

}

func (entry *Entry) Errorf(format string, args ...interface{}) {
	entry.Logf(logrus.ErrorLevel, format, args...)
}

func (entry *Entry) Fatalf(format string, args ...interface{}) {
	entry.Logf(logrus.FatalLevel, format, args...)
	os.Exit(1)

}

func (entry *Entry) Panicf(format string, args ...interface{}) {
	entry.Logf(logrus.PanicLevel, format, args...)
}

func (entry *Entry) Traceln(args ...interface{}) {
	entry.Logln(logrus.TraceLevel, args...)
}

func (entry *Entry) Debugln(args ...interface{}) {
	entry.Logln(logrus.DebugLevel, args...)
}

func (entry *Entry) Infoln(args ...interface{}) {
	entry.Logln(logrus.InfoLevel, args...)
}

func (entry *Entry) Println(args ...interface{}) {
	entry.Infoln(args...)
}

func (entry *Entry) Warnln(args ...interface{}) {
	entry.Logln(logrus.WarnLevel, args...)
}

func (entry *Entry) Warningln(args ...interface{}) {
	entry.Warnln(args...)
}

func (entry *Entry) Errorln(args ...interface{}) {
	entry.Logln(logrus.ErrorLevel, args...)
}

func (entry *Entry) Fatalln(args ...interface{}) {
	entry.Logln(logrus.FatalLevel, args...)
	os.Exit(1)
}

func (entry *Entry) Panicln(args ...interface{}) {
	entry.Logln(logrus.PanicLevel, args...)
}
