{{ $pkg := .Pkg}}
package router

import (
	"github.com/gin-gonic/gin"
	"{{.Pkg }}/core/middlewares"
	"{{.Pkg }}/core/admin"
	{{ range .Apps }}
	"{{ $pkg }}/{{.}}"
	{{ end }}
	{{if .Swagger}}
	_ "{{ $pkg }}/docs"
	{{end}}
	log "github.com/go-eden/slf4go"
	"github.com/gin-contrib/gzip"
)

// 注册插件和路由
func PreGinServe(r *gin.Engine) {
	{{if .Admin}}
	// admin website
	admin.RegisterAdmin(r)

	// static fs
	admin.RegisterStatic(r)
	{{end}}

	log.Info("注册Gin路由")
	r.Use(middlewares.TokenFilter())
	r.Use(gin.Logger())
	r.Use(gin.Recovery())
	r.Use(middlewares.Cors())

	// 添加prometheus监控
	middlewares.RegisterPrometheusMiddleware(r, false)

	// gzip
	r.Use(gzip.Gzip(gzip.DefaultCompression, gzip.WithExcludedPathsRegexs([]string{".*"})))

	// 404
	r.NoRoute(middlewares.NoRouteHandler)

	r.GET("/",func(c *gin.Context) {
		c.Redirect(301,"/admin/index")
	})

	// 健康检查
	r.GET("/health", middlewares.RegisterHealthMiddleware)

	{{if .Swagger }}
	// Swagger
	middlewares.RegisterSwaggerMiddleware(r)
	{{end}}

	{{if .Jwt}}
	middlewares.RegisterAuth(r)
	{{end}}

	// 自动注册，请勿修改
	{{ range .Apps }}
	{{.}}.Register{{ . }}(r)
	{{ end }}
}
