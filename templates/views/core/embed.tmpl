package admin

import (
	"{{.Pkg}}/utils"
	"embed"
	"html/template"
	"net/http"
	"path/filepath"

	"github.com/gin-contrib/multitemplate"
	"github.com/gin-gonic/gin"
)

//go:embed static
var static embed.FS

//go:embed templates
var templates embed.FS

// 注册静态文件和templates模板
// 参考： https://www.gouguoyin.cn/posts/10103.html
func RegisterStatic(router *gin.Engine) {
	// 注册静态文件
	router.StaticFS("/adminfs", http.FS(static))

	// 基于embed注册templates模板
	t := template.Must(template.New("").Funcs(utils.FuncMap).ParseFS(templates, "templates/*/*"))
	router.SetHTMLTemplate(t)

	// 基于本地Files注册templates模板
	// router.SetFuncMap(utils.FuncMap)
	// router.LoadHTMLGlob("core/admin/templates/**/*")

	// 其它方案
	// router.HTMLRender = multitemplate.Renderer()
	// router.HTMLRender = LoadTemplates("core/admin/templates")
}

func LoadTemplates(templatesDir string) multitemplate.Renderer {
	r := multitemplate.NewRenderer()

	// 非模板嵌套
	// htmls, err := filepath.Glob(templatesDir + "/htmls/*.html")
	// if err != nil {
	// 	panic(err.Error())
	// }
	// for _, html := range htmls {
	// 	r.AddFromGlob(filepath.Base(html), html)
	// }

	// 布局模板
	layouts, err := filepath.Glob(templatesDir + "/layout/*.html")
	if err != nil {
		panic(err.Error())
	}

	// 嵌套的内容模板
	admins, err := filepath.Glob(templatesDir + "/**/*.html")
	if err != nil {
		panic(err.Error())
	}

	// template自定义函数
	// funcMap := template.FuncMap{
	// 	"StringToLower": func(str string) string {
	// 		return strings.ToLower(str)
	// 	},
	// }

	// 将主模板，include页面，layout子模板组合成一个完整的html页面
	for _, include := range admins {
		files := []string{}
		// index.tmpl主模板
		files = append(files, templatesDir+"/index.tmpl", include)
		files = append(files, layouts...)
		r.AddFromFilesFuncs(filepath.Base(include), utils.FuncMap, files...)
	}

	return r
}
