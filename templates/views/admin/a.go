package admin

// import (
// 	"fmt"
// 	"html"
// 	"html/template"
// 	"net/http"
// 	"path/filepath"
// 	"strings"

// 	"github.com/gin-contrib/multitemplate"
// 	"github.com/gin-gonic/gin"
// )

// var engine *gin.Engine

// func loadTemplates(templatesDir string, module ...string) multitemplate.Renderer {
// 	r := multitemplate.NewRenderer()
// 	for _, moduleName := range module {
// 		layouts, err := filepath.Glob("./" + templatesDir + "/" + moduleName + "/*.html")
// 		if err != nil {
// 			panic(err.Error())
// 		}
// 		includes, err := filepath.Glob("./" + templatesDir + "/" + moduleName + "/**/*.html")
// 		if err != nil {
// 			panic(err.Error())
// 		}
// 		for _, include := range includes {
// 			fmt.Println(layouts)
// 			layoutCopy := make([]string, len(layouts))
// 			copy(layoutCopy, layouts)
// 			files := append(layoutCopy, include) //给模板添加自定义函数
// 			r.AddFromFilesFuncs(strings.Replace(filepath.ToSlash(include), templatesDir, "", -1), template.FuncMap{"xss": xss, "adminUrl": adminUrl, "url": url, "myFunc": myFunc}, files...)
// 		}
// 	}
// 	return r
// }
// func xss(a string) string         { return html.EscapeString(a) }
// func adminUrl(path string) string { return "/admin/" + strings.Trim(path, "/") }
// func url(path string) string      { return "/admin/" + strings.Trim(path, "/") }
// func myFunc(s string) string      { return s + "myFunc" }
// func init() {
// 	engine = gin.Default() //设置模板渲染
// 	engine.HTMLRender = loadTemplates("templates", "web", "admin")
// }

// //router router
// func router() *gin.Engine { return engine }
// func main() {
// 	router := router()
// 	router.GET("/a", func(c *gin.Context) { //加载名称为/web/a/index.html的模板，并给模板赋值
// 		c.HTML(http.StatusOK, "/web/a/index.html", gin.H{"title": "gin框架之HTML模板渲染-a"})
// 	})
// 	router.GET("/b", func(c *gin.Context) { //加载名称为/web/b/index.html，并给模板赋值
// 		c.HTML(http.StatusOK, "/web/b/index.html", gin.H{"title": "gin框架之HTML模板渲染-b"})
// 	})
// 	router.GET("/func", func(c *gin.Context) { //加载名称为/web/func/index.html，并给模板赋值
// 		c.HTML(http.StatusOK, "/web/func/index.html", gin.H{"title": "gin框架之HTML模板渲染- "})
// 	})
// 	router.Run(":8080")
// }
