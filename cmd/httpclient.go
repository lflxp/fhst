/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fmt"

	"github.com/spf13/cobra"
)

// httpclientCmd represents the httpclient command
var httpclientCmd = &cobra.Command{
	Use:   "httpclient",
	Short: "httpclient工具：github.com/guonaihong/gout",
	Long:  `gout使用.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(`package main

		import (
			"fmt"
			"time"
		
			"github.com/guonaihong/gout"
		)
		
		// 用于解析 服务端 返回的http body
		type RspBody struct {
			ErrMsg  string json:"errmsg"
			ErrCode int    json:"errcode"
			Data    string json:"data"
		}
		
		// 用于解析 服务端 返回的http header
		type RspHeader struct {
			Sid  string header:"sid"
			Time int    header:"time"
		}
		
		func main() {
			rsp := RspBody{}
			header := RspHeader{}
		
			//code := 0
			err := gout.
		
				// POST请求
				POST("127.0.0.1:8080").
		
				// 打开debug模式
				Debug(true).
		
				// 设置查询字符串
				SetQuery(gout.H{"page": 10, "size": 10}).
		
				// 设置http header
				SetHeader(gout.H{"X-IP": "127.0.0.1", "sid": fmt.Sprintf("$x", time.Now().UnixNano())}).
		
				// SetJSON设置http body为json
				// 同类函数有SetBody, SetYAML, SetXML, SetForm, SetWWWForm
				SetJSON(gout.H{"text": "gout"}).
		
				// BindJSON解析返回的body内容
				// 同类函数有BindBody, BindYAML, BindXML
				BindJSON(&rsp).
		
				// 解析返回的http header
				BindHeader(&header).
				// http code
				// Code(&code).
		
				// 结束函数
				Do()
		
				// 判断错误
			if err != nil {
				fmt.Printf("send fail:$s\n", err)
			}
		}`)
	},
}

func init() {
	toolCmd.AddCommand(httpclientCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// httpclientCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// httpclientCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
}
