/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fhst/pkg/app"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var isJwt bool

// startappCmd represents the startapp command
var startappCmd = &cobra.Command{
	Use:   "startapp",
	Short: "创建应用",
	Long: `fhst startapp app1. For example:

Cobra is a CLI library for Go that empowers applications.
This application is a tool to generate the needed files
to quickly create a Cobra application.`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Errorln("请指定创建app的名称!!!")
			return
		}

		for _, newapp := range args {
			err := app.CreateApp(newapp, isJwt)
			if err != nil {
				log.Fatalln(err)
			}
		}

		// 判断admin文件夹是否存在
		defer func() {
			err := app.CreateAdmin()
			if err != nil {
				panic(err)
			}
			log.Infof("初始化Admin 完成")
		}()
	},
}

func init() {
	rootCmd.AddCommand(startappCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// startappCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// startappCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	startappCmd.Flags().BoolVar(&isJwt, "jwt", false, "是否创建带JWT认证的app")
}
