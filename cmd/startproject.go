/*
Copyright © 2021 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
	"fhst/pkg/project"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var (
	pkg string // go mod init pkg
)

// startprojectCmd represents the startproject command
var startprojectCmd = &cobra.Command{
	Use:   "startproject",
	Short: "创建项目",
	Long: `创建一个Gin API的项目. For example:

创建一个项目：fhst startproject project1
指定初始化mod的仓库名： fhst startproject1 --pkg github.com/lflxp/xxx`,
	Run: func(cmd *cobra.Command, args []string) {
		if len(args) == 0 {
			log.Errorln("请指定创建项目的名称!!!")
			return
		}

		log.Infof("初始化项目: %s", args[0])
		err := project.CreateProject(args[0], pkg)
		if err != nil {
			log.Fatalln(err.Error())
		} else {
			log.Infof("project %s create success", args[0])
		}
	},
}

func init() {
	rootCmd.AddCommand(startprojectCmd)

	// Here you will define your flags and configuration settings.

	// Cobra supports Persistent Flags which will work for this command
	// and all subcommands, e.g.:
	// startprojectCmd.PersistentFlags().String("foo", "", "A help for foo")

	// Cobra supports local flags which will only run when this command
	// is called directly, e.g.:
	// startprojectCmd.Flags().BoolP("toggle", "t", false, "Help message for toggle")
	startprojectCmd.Flags().StringVar(&pkg, "pkg", "demo", "go mod pkg name")
}
