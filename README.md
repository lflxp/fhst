# 介绍

fhst == Faster Higher Stronger Together
平时开发使用gin总是重复创建和搭建框架，这个项目的目的实现Gin框架的CLI工具脚手架，快速创建任务。
设计来源：
* Django-admin
* Cobra

# 安装

> go get gitee.com/lflxp/fhst

# 使用

1. 创建项目： fhst startproject test
2. 创建API应用： cd test && fhst startapp app1
3. 运行： make all

# 测试

测试会按照makefile测试流程跑完常规操作，建立一个完整的api app应用

> make test

# http client

`推荐使用(功能全、拿来即用)`: https://github.com/guonaihong/gout

# log插件

`推荐使用`: https://github.com/go-eden/slf4go

# TODO

1. [x] jwt yaml配置
    1. jwt 登陆接口
    2. jwt 退出接口
    3. jwt refresh_token 接口
2. trace
3. sql orm
4. mq接入
5. allinone 二进制合并静态文件 包括swagger docs和前端dist以及static js文件
6. django-admin website + go template framework
    1. cankao: gitee.com/lflxp/beegoadmin
    2. go template funcMap => https://blog.csdn.net/shida_csdn/article/details/113760434
    3. todo: add static and controllers/default.go
7. 通过//go:generate 方式给Model进行分组 => 通过model本级文件目录分组
8. jwt对接admin model user
9. connector =》 neo4j meilisearch elasticsearch
10. xsrf
11. template html => https://jishuin.proginn.com/p/763bfbd3aa2e
12. 添加gitee和gitserver的sdk和对应的model和controller
13. 添加app和admin的metrics监
14. 美化fhst报错提示 目前使用panic

# 参考

> https://blog.csdn.net/EDDYCJY/article/details/113577812
> https://www.cnblogs.com/lczmx/p/13268081.html
> httpclient: https://github.com/guonaihong/gout | https://gitee.com/guonaihong/gout

> https://github.com/meilisearch/meilisearch-go
