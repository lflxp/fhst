package utils

import (
	"fmt"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func init() {
	if host := viper.GetString("host"); host != "" {
		viper.SetDefault("host", host)
	} else {
		viper.SetDefault("HOST", "0.0.0.0")
	}

	if port := viper.GetString("port"); port != "" {
		viper.SetDefault("port", port)
	} else {
		viper.SetDefault("PORT", "8000")
	}

}

// 设置项目信息
func SetAppYaml(value, name string) error {
	// 获取现有项目信息
	apps := viper.GetStringSlice("app")

	log.Debugf("apps is %v, all settings %v", apps, viper.AllSettings())
	if apps == nil {
		log.Debugln("apps is nil")
		viper.Set("app", []string{value})
	} else {
		apps = append(apps, value)
		viper.Set("app", apps)
	}

	viper.WriteConfigAs(fmt.Sprintf("%s.yaml", name))
	return nil
}
