package tool

import (
	"errors"
	"fhst/cache"
	p "fhst/templates"
	"fhst/utils"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func AddAdmin() error {
	defer func() {
		info := utils.History{
			Name:   "AdminWebSite",
			Op:     "开启Admin",
			Common: "fhst tool admin",
		}
		_, err := utils.AddHistory(&info)
		if err != nil {
			log.Error(err)
		}
	}()

	// 数据库初始化
	utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("未发现.fhst文件，请回到项目根目录")
	}

	// if utils.IsPathExists("app") {
	// 	return errors.New("app文件夹已存在")
	// }

	log.Infof("初始化app目录")
	// 创建核心文件夹
	_, err := utils.ExecCommandString("mkdir -p app/static/css && mkdir -p app/static/fonts && mkdir -p app/static/js && mkdir -p app/views/admin && mkdir -p app/views/layout")
	if err != nil {
		return err
	}

	rs, err := utils.ReadFile(".fhst")
	if err != nil {
		return err
	}

	pkgname := strings.Split(string(rs), ":")
	// data := map[string]interface{}{
	// 	"Pkg":         pkgname[1],
	// 	"ProjectName": pkgname[0],
	// 	"Log":         true,
	// 	"Admin":       true,
	// 	"Apps":        viper.GetStringSlice("app"),
	// 	"Jwt":         viper.GetBool("jwt"),
	// 	"Swagger":     viper.GetBool("swagger"),
	// 	"Meili":       viper.GetBool("meili.enable"),
	// }

	// 写入配置文件
	defer func() {
		viper.Set("admin", true)
		viper.WriteConfigAs(fmt.Sprintf("%s.yaml", pkgname[0]))
	}()

	// log.Infof("初始化Swagger路由表")
	// err = utils.WriteFileByTemplate(p.Project, "project/register.tmpl", "core/router/register.go", data)
	// if err != nil {
	// 	return err
	// }

	log.Infof("初始化index.tmpl")
	err = utils.WriteFileByTemplate(p.Views, "views/index.tmpl", "app/views/admin/index.tmpl", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化add.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/add.html", "app/views/admin/add.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化edit.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/edit.html", "app/views/admin/edit.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化login.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/login.html", "app/views/admin/login.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化table.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/table.html", "app/views/admin/table.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化home.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/home.html", "app/views/admin/home.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化test.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/test.html", "app/views/admin/test.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 nav.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/nav.html", "app/views/layout/nav.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 footer.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/footer.html", "app/views/layout/footer.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 header.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/header.html", "app/views/layout/header.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 headereasy.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/headereasy.html", "app/views/layout/headereasy.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 menu.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/menu.html", "app/views/layout/menu.html", nil)
	if err != nil {
		return err
	}

	log.Infof("初始化 headertable.html")
	err = utils.WriteFileByTemplate(p.Views, "views/admin/layout/headertable.html", "app/views/layout/headertable.html", nil)
	if err != nil {
		return err
	}

	// TODO: Add Static Files
	log.Infof("初始化static文件夹")
	css, err := cache.Static.ReadDir("static/css")
	if err != nil {
		panic(err)
	}

	for _, fs := range css {
		if !fs.IsDir() {
			log.Infof("Static Css 文件同步 %s %v %s", fs.Name(), fs.IsDir(), fs.Type().String())
			err = utils.WriteFileByTemplate(cache.Static, fmt.Sprintf("static/css/%s", fs.Name()), fmt.Sprintf("app/static/css/%s", fs.Name()), nil)
			if err != nil {
				return err
			}
		}
	}

	fonts, err := cache.Static.ReadDir("static/fonts")
	if err != nil {
		panic(err)
	}

	for _, fs := range fonts {
		if !fs.IsDir() {
			log.Infof("Static Fonts 文件同步 %s %v %s", fs.Name(), fs.IsDir(), fs.Type().String())
			err = utils.WriteFileByTemplate(cache.Static, fmt.Sprintf("static/fonts/%s", fs.Name()), fmt.Sprintf("app/static/fonts/%s", fs.Name()), nil)
			if err != nil {
				return err
			}
		}
	}

	fss, err := cache.Static.ReadDir("static/js")
	if err != nil {
		panic(err)
	}

	for _, fs := range fss {
		if !fs.IsDir() {
			log.Infof("Static Js 文件同步 %s %v %s", fs.Name(), fs.IsDir(), fs.Type().String())
			err = utils.WriteFileByTemplate(cache.Static, fmt.Sprintf("static/js/%s", fs.Name()), fmt.Sprintf("app/static/js/%s", fs.Name()), nil)
			if err != nil {
				return err
			}
		}
	}

	// log.Infof("初始化embed.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/core/embed.tmpl", "app/embed.go", data)
	// if err != nil {
	// 	return err
	// }

	// log.Infof("初始化table.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/table.tmpl", "utils/table.go", nil)
	// if err != nil {
	// 	return err
	// }

	// log.Infof("初始化filter.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/filter.tmpl", "utils/filter.go", data)
	// if err != nil {
	// 	return err
	// }

	// log.Infof("初始化 controller.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/core/controller.tmpl", "app/controller.go", data)
	// if err != nil {
	// 	return err
	// }

	// log.Infof("初始化 model.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/admin/model/model.tmpl", "app/model/model.go", data)
	// if err != nil {
	// 	return err
	// }

	// log.Infof("初始化 register.go")
	// err = utils.WriteFileByTemplate(p.Views, "views/admin/model/register.tmpl", "app/model/register.go", data)
	// if err != nil {
	// 	return err
	// }

	return nil
}
