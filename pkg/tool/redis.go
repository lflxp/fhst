package tool

import (
	"errors"
	p "fhst/templates"
	"fhst/utils"

	log "github.com/sirupsen/logrus"
)

// 添加线程安全的map缓存
func AddRedis() error {
	defer func() {
		info := utils.History{
			Name:   "开启redis",
			Op:     "添加redis缓存",
			Common: "fhst tool redis",
		}
		_, err := utils.AddHistory(&info)
		if err != nil {
			log.Error(err)
		}
	}()

	// 数据库初始化
	utils.ConnectInit()

	// 判断当前位置是否有core文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("未发现.fhst文件，请回到项目根目录")
	}

	if utils.IsPathExists("utils/redis.go") {
		return errors.New("redis.go已经存在")
	}

	log.Infof("初始化redis")
	err := utils.WriteFileByTemplate(p.App, "app/redis.tmpl", "utils/redis.go", nil)
	return err
}
