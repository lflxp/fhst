package tool

import (
	"errors"
	p "fhst/templates"
	"fhst/utils"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// 添加线程安全的map缓存
func AddXorm() error {
	defer func() {
		info := utils.History{
			Name:   "开启Xorm",
			Op:     "添加xorm工具接入",
			Common: "fhst tool xorm",
		}
		_, err := utils.AddHistory(&info)
		if err != nil {
			log.Error(err)
		}
	}()

	// 数据库初始化
	utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("未发现.fhst文件，请回到项目根目录")
	}

	if !utils.IsPathExists("utils/xorm.go") {
		rs, err := utils.ReadFile(".fhst")
		if err != nil {
			return err
		}

		pkgname := strings.Split(string(rs), ":")
		data := map[string]interface{}{
			"Pkg":     pkgname[1],
			"Name":    pkgname[0],
			"Apps":    viper.GetStringSlice("app"),
			"Swagger": viper.GetBool("swagger"),
			"Jwt":     viper.GetBool("jwt"),
			"Admin":   viper.GetBool("admin"),
		}

		log.Infof("初始化Xorm工具")
		err = utils.WriteFileByTemplate(p.Tool, "tool/xorm.tmpl", "utils/xorm.go", data)
		return err
	}
	return nil
}
