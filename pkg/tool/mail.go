package tool

import (
	"errors"
	p "fhst/templates"
	"fhst/utils"

	log "github.com/sirupsen/logrus"
)

// 添加线程安全的map缓存
func AddMail() error {
	defer func() {
		info := utils.History{
			Name:   "开启JWT",
			Op:     "添加发送email的工具",
			Common: "fhst tool mail",
		}
		_, err := utils.AddHistory(&info)
		if err != nil {
			log.Error(err)
		}
	}()

	// 数据库初始化
	utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("未发现.fhst文件，请回到项目根目录")
	}

	if !utils.IsPathExists("utils/mail.go") {
		log.Infof("初始化Mail工具")
		err := utils.WriteFileByTemplate(p.Tool, "tool/mail.tmpl", "utils/mail.go", nil)
		return err
	}
	return nil
}
