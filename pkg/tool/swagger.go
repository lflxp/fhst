package tool

import (
	"errors"
	p "fhst/templates"
	"fhst/utils"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

func AddSwagger() error {
	defer func() {
		info := utils.History{
			Name:   "开启swagger",
			Op:     "添加swagger",
			Common: "fhst tool swagger",
		}
		_, err := utils.AddHistory(&info)
		if err != nil {
			log.Error(err)
		}
	}()

	// 数据库初始化
	utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("未发现.fhst文件，请回到项目根目录")
	}

	if utils.IsPathExists("core/middlewares/swagger.go") {
		return errors.New("swagger.go已存在")
	}

	rs, err := utils.ReadFile(".fhst")
	if err != nil {
		return err
	}

	pkgname := strings.Split(string(rs), ":")
	data := map[string]interface{}{
		"Pkg":     pkgname[1],
		"Name":    pkgname[0],
		"Apps":    viper.GetStringSlice("app"),
		"Swagger": true,
		"Jwt":     viper.GetBool("jwt"),
		"Admin":   viper.GetBool("admin"),
	}

	// 写入配置文件
	defer func() {
		viper.Set("swagger", true)
		viper.WriteConfigAs(fmt.Sprintf("%s.yaml", pkgname[0]))
	}()

	log.Infof("初始化Swagger路由表")
	err = utils.WriteFileByTemplate(p.Project, "project/register.tmpl", "core/router/register.go", data)
	if err != nil {
		return err
	}

	// 创建swagger文件
	err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/swagger.tmpl", "core/middlewares/swagger.go", nil)
	if err != nil {
		return err
	}
	return nil
}
