package app

import (
	"errors"
	"fhst/pkg/tool"
	p "fhst/templates"
	"fhst/utils"
	"strings"

	"github.com/spf13/viper"
)

// 创建APP应用
func CreateAdmin() error {
	// defer func() {
	// 	info := utils.History{
	// 		Name:   fmt.Sprintf("新增app %s", name),
	// 		Op:     "fhst startapp",
	// 		Common: fmt.Sprintf("isjwt %v", isjwt),
	// 	}
	// 	_, err := utils.AddHistory(&info)
	// 	if err != nil {
	// 		log.Error(err)
	// 	}
	// }()

	// 数据库初始化
	// utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("请回到项目根目录执行添加APP")
	}

	rs, err := utils.ReadFile(".fhst")
	if err != nil {
		return err
	}

	pkgname := strings.Split(string(rs), ":")
	tmpapp := viper.GetStringSlice("app")
	data := map[string]interface{}{
		"Pkg":         pkgname[1],
		"Name":        pkgname[0],
		"ProjectName": pkgname[0],
		"Apps":        tmpapp,
		"Jwt":         viper.GetBool("jwt"),
		"Swagger":     viper.GetBool("swagger"),
		"Log":         true,
		"Admin":       viper.GetBool("admin"),
	}

	// 创建cache xorm
	// tool.AddXorm()
	// tool.AddConcurrentMap()
	// tool.AddRedis()
	err = tool.AddAdmin()
	if err != nil {
		panic(err)
	}

	// 创建核心文件夹
	_, err = utils.ExecCommandString("mkdir -p app/controller app/model/admin")
	if err != nil {
		return err
	}

	err = utils.WriteFileByTemplate(p.Controller, "controller/admin.tmpl", "app/controller/admin_controller.go", data)
	if err != nil {
		return err
	}

	err = utils.WriteFileByTemplate(p.Model, "model/admin.tmpl", "app/model/admin/admin.go", data)
	if err != nil {
		return err
	}

	err = utils.WriteFileByTemplate(p.App, "app/middlewares.tmpl", "app/middlewares.go", data)
	if err != nil {
		return err
	}

	err = utils.WriteFileByTemplate(p.App, "app/embed.tmpl", "app/embed.go", data)
	if err != nil {
		return err
	}

	return nil
}
