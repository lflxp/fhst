package app

import (
	"errors"
	p "fhst/templates"
	"fhst/utils"
	"fmt"
	"strings"

	log "github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

// 创建APP应用
func CreateApp(name string, isjwt bool) error {
	// defer func() {
	// 	info := utils.History{
	// 		Name:   fmt.Sprintf("新增app %s", name),
	// 		Op:     "fhst startapp",
	// 		Common: fmt.Sprintf("isjwt %v", isjwt),
	// 	}
	// 	_, err := utils.AddHistory(&info)
	// 	if err != nil {
	// 		log.Error(err)
	// 	}
	// }()

	// 数据库初始化
	// utils.ConnectInit()

	// 判断当前位置是否有utils文件夹
	if !utils.IsPathExists(".fhst") {
		return errors.New("请回到项目根目录执行添加APP")
	}

	rs, err := utils.ReadFile(".fhst")
	if err != nil {
		return err
	}

	pkgname := strings.Split(string(rs), ":")
	tmpapp := viper.GetStringSlice("app")
	tmpapp = append(tmpapp, name)
	data := map[string]interface{}{
		"Pkg":         pkgname[1],
		"Name":        pkgname[0],
		"ProjectName": pkgname[0],
		"App":         name,
		"Apps":        tmpapp,
		"Jwt":         viper.GetBool("jwt"),
		"Swagger":     viper.GetBool("swagger"),
		"Log":         true,
		"Admin":       viper.GetBool("admin"),
	}

	// 同步信息
	defer func() {
		// 同步全局配置信息
		viper.Set("global", map[string]string{"Pkg": pkgname[1], "Name": pkgname[0]})
		// 同步项目信息
		utils.SetAppYaml(name, pkgname[0])
	}()

	// 创建cache xorm
	// tool.AddXorm()
	// tool.AddConcurrentMap()
	// tool.AddRedis()

	log.Infof("初始化APP项目 %s", name)
	// 创建核心文件夹
	_, err = utils.ExecCommandString(fmt.Sprintf("mkdir -p app/controller app/model/%s app/static app/views/%s", name, name))
	if err != nil {
		return err
	}

	err = utils.WriteFileByTemplate(p.Controller, "controller/controller.tmpl", fmt.Sprintf("app/controller/%s_controller.go", name), data)
	if err != nil {
		return err
	}

	log.Infof("创建App Model %s", name)
	err = utils.WriteFileByTemplate(p.Model, "model/model.tmpl", fmt.Sprintf("app/model/%s/%s.go", name, name), data)
	if err != nil {
		return err
	}

	return nil
}
