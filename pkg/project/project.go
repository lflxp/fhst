package project

import (
	p "fhst/templates"
	"fhst/utils"
	"fmt"

	log "github.com/sirupsen/logrus"
)

// 初始化项目
func CreateProject(name, pkgName string) error {
	data := map[string]interface{}{
		"Pkg":         pkgName,
		"ProjectName": name,
	}
	log.Infof("初始化项目框架")

	// 创建核心文件夹
	_, err := utils.ExecCommandString(fmt.Sprintf("mkdir -p %s/utils %s/cmd", name, name))
	if err != nil {
		return err
	}

	log.Info("创建初始化配置文件")
	_, err = utils.WriteFile(fmt.Sprintf("%s/.fhst", name), []byte(fmt.Sprintf("%s:%s", name, pkgName)))
	if err != nil {
		return err
	}
	// 创建makefile通用文件
	log.Infof("初始化Makefile通用文件")
	err = utils.WriteFileByTemplate(p.Project, "project/makefile.tmpl", fmt.Sprintf("%s/Makefile", name), nil)
	if err != nil {
		return err
	}
	// =======================common
	// 创建common通用文件
	log.Infof("初始化common通用文件")
	err = utils.WriteFileByTemplate(p.Tool, "tool/common.tmpl", fmt.Sprintf("%s/utils/common.go", name), nil)
	if err != nil {
		return err
	}
	// =======================middlewares
	// 创建cors文件
	// log.Infof("初始化中间件")
	// err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/cors.tmpl", fmt.Sprintf("%s/core/middlewares/cors.go", name), nil)
	// if err != nil {
	// 	return err
	// }
	// err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/filter.tmpl", fmt.Sprintf("%s/core/middlewares/filter.go", name), nil)
	// if err != nil {
	// 	return err
	// }
	// // 创建health文件
	// err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/health.tmpl", fmt.Sprintf("%s/core/middlewares/health.go", name), nil)
	// if err != nil {
	// 	return err
	// }
	// // 创建noroute文件
	// err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/noroute.tmpl", fmt.Sprintf("%s/core/middlewares/noroute.go", name), nil)
	// if err != nil {
	// 	return err
	// }
	// // 创建prometheus文件
	// err = utils.WriteFileByTemplate(p.Middlewares, "middlewares/prometheus.tmpl", fmt.Sprintf("%s/core/middlewares/prometheus.go", name), nil)
	// if err != nil {
	// 	return err
	// }

	// ======================Project
	log.Info("初始化项目核心文件")
	// 创建main文件
	err = utils.WriteFileByTemplate(p.Project, "project/main.tmpl", fmt.Sprintf("%s/main.go", name), data)
	if err != nil {
		return err
	}

	// 创建yaml配置文件
	err = utils.WriteFileByTemplate(p.Project, "project/conf.tmpl", fmt.Sprintf("%s/%s.yaml", name, name), nil)
	if err != nil {
		return err
	}

	// 创建setting配置文件
	// err = utils.WriteFileByTemplate(p.Project, "project/setting.tmpl", fmt.Sprintf("%s/core/settings.go", name), data)
	// if err != nil {
	// 	return err
	// }

	// 创建root.go
	err = utils.WriteFileByTemplate(p.Project, "project/root.tmpl", fmt.Sprintf("%s/cmd/root.go", name), data)
	if err != nil {
		return err
	}

	// 创建migrate.go
	// err = utils.WriteFileByTemplate(p.Project, "project/migrate.tmpl", fmt.Sprintf("%s/cmd/migrate.go", name), nil)
	// if err != nil {
	// 	return err
	// }

	// 创建register.go
	// err = utils.WriteFileByTemplate(p.Project, "project/register.tmpl", fmt.Sprintf("%s/core/router/register.go", name), data)
	// if err != nil {
	// 	return err
	// }

	log.Infof("初始化项目mod module %s project %s\n初始化 %s 中...", pkgName, name, fmt.Sprintf("go mod init %s && go mod tidy && go get -u github.com/go-eden/routine gitee.com/lflxp/dogo", pkgName))
	// 初始化go mod
	rs, err := utils.ExecCommandGitWithRootAndOutput(fmt.Sprintf("go mod init %s && go mod tidy && go get -u github.com/go-eden/routine gitee.com/lflxp/dogo", pkgName), name)
	if err != nil {
		return err
	}
	log.Debugln(rs)

	return nil
}
