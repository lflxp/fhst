package test

import (
	"testing"

	. "fhst/utils"

	. "github.com/agiledragon/gomonkey"
	. "github.com/smartystreets/goconvey/convey"
)

func TestDemo(t *testing.T) {
	Convey("Given some integer with a starting value", t, func() {
		x := 1
		Convey("When the int is incremented", func() {
			x++
			Convey("The value should be greater by one", func() {
				So(x, ShouldEqual, 2)
			})
		})
	})
}

func Test_ApplyTemplate(t *testing.T) {
	temp := `{{.Name}}`
	Convey("Test Go Template Func", t, func() {
		Convey("ApplyTemplate", func() {
			patch := ApplyFunc(ApplyTemplate, func(_ string, _ map[string]interface{}) ([]byte, error) {
				return []byte("Hello"), nil
			})
			defer patch.Reset()

			data := map[string]interface{}{"Name": "Hello"}
			out, err := ApplyTemplate(temp, data)
			t.Logf("out is %s", string(out))
			So(err, ShouldEqual, nil)
			So(string(out), ShouldEqual, "Hello")
		})

		Convey("ExecCommand", func() {
			patch := ApplyFunc(ExecCommand, func(_ string) ([]byte, error) {
				return []byte("world"), nil
			})
			defer patch.Reset()

			rs, err := ExecCommand("echo 'world'")
			t.Logf("rs is %s", rs)
			So(err, ShouldEqual, nil)
			So(string(rs), ShouldEqual, "world")
		})

		Convey("ExecCommandString", func() {
			patch := ApplyFunc(ExecCommandString, func(_ string) (string, error) {
				return "world", nil
			})
			defer patch.Reset()

			rs, err := ExecCommandString("echo 'world'")
			t.Logf("ExecCommandString rs is %s", rs)
			So(err, ShouldEqual, nil)
			So(rs, ShouldEqual, "world")
		})

		Convey("IsPathExists", func() {
			patch := ApplyFunc(IsPathExists, func(_ string) bool {
				return true
			})
			defer patch.Reset()

			rs := IsPathExists("/tmp")
			t.Logf("IsPathExists rs is %v", rs)
			So(rs, ShouldBeTrue)
		})
	})
}
