module fhst

go 1.16

require (
	gitee.com/lflxp/dogo v0.0.9
	github.com/agiledragon/gomonkey v2.0.2+incompatible
	github.com/bketelsen/crypt v0.0.4 // indirect
	github.com/fsnotify/fsnotify v1.6.0 // indirect
	github.com/gin-gonic/gin v1.8.1
	github.com/go-eden/slf4go v1.1.2 // indirect
	github.com/go-playground/validator/v10 v10.11.1 // indirect
	github.com/goccy/go-json v0.9.11 // indirect
	github.com/guonaihong/gout v0.3.1 // indirect
	github.com/mattn/go-isatty v0.0.16 // indirect
	github.com/mattn/go-sqlite3 v1.14.15
	github.com/matttproud/golang_protobuf_extensions v1.0.2 // indirect
	github.com/prometheus/client_golang v1.13.0 // indirect
	github.com/sirupsen/logrus v1.8.1
	github.com/smartystreets/goconvey v1.6.4
	github.com/spf13/afero v1.9.2 // indirect
	github.com/spf13/cobra v1.4.0
	github.com/spf13/viper v1.13.0
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/net v0.0.0-20221017152216-f25eb7ecb193 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	xorm.io/builder v0.3.12 // indirect
	xorm.io/core v0.7.3
	xorm.io/xorm v1.3.2
)
