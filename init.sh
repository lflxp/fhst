#!/bin/sh
set -e


echo '常用软件安装tmux autojump gowatch showme git golang vim python pip vscode tree make g++'
getos() {
	if [ -f /etc/debian_version ];
	then
		echo 'Debian系统'
		sudo apt-get install zsh tmux autojump git vim python3-pip tree cmake make g++ -y 
		echo '配置ubunut 20.04阿里源'
		cat << EOF > /etc/apt/sources.list.d/aliyun.list
deb http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-security main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-updates main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-proposed main restricted universe multiverse
deb http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
deb-src http://mirrors.aliyun.com/ubuntu/ focal-backports main restricted universe multiverse
EOF
		sudo apt-get update

	elif [ -f /etc/centos-release ];
	then
		echo 'Centos系统'
		sudo yum install zsh tmux autojump git vim python3-pip tree cmake make g++ -y
		echo '配置centos 7 阿里源'
# 		cat << EOF > /etc/yum.repos.d/CentOS-Base.repo
# # CentOS-Base.repo
# #
# # The mirror system uses the connecting IP address of the client and the
# # update status of each mirror to pick mirrors that are updated to and
# # geographically close to the client.  You should use this for CentOS updates
# # unless you are manually picking other mirrors.
# #
# # If the mirrorlist= does not work for you, as a fall back you can try the
# # remarked out baseurl= line instead.
# #
# #

# [base]
# name=CentOS-$releasever - Base - mirrors.aliyun.com
# failovermethod=priority
# baseurl=http://mirrors.aliyun.com/centos/$releasever/os/$basearch/
#         http://mirrors.aliyuncs.com/centos/$releasever/os/$basearch/
#         http://mirrors.cloud.aliyuncs.com/centos/$releasever/os/$basearch/
# gpgcheck=1
# gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

# #released updates
# [updates]
# name=CentOS-$releasever - Updates - mirrors.aliyun.com
# failovermethod=priority
# baseurl=http://mirrors.aliyun.com/centos/$releasever/updates/$basearch/
#         http://mirrors.aliyuncs.com/centos/$releasever/updates/$basearch/
#         http://mirrors.cloud.aliyuncs.com/centos/$releasever/updates/$basearch/
# gpgcheck=1
# gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

# #additional packages that may be useful
# [extras]
# name=CentOS-$releasever - Extras - mirrors.aliyun.com
# failovermethod=priority
# baseurl=http://mirrors.aliyun.com/centos/$releasever/extras/$basearch/
#         http://mirrors.aliyuncs.com/centos/$releasever/extras/$basearch/
#         http://mirrors.cloud.aliyuncs.com/centos/$releasever/extras/$basearch/
# gpgcheck=1
# gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

# #additional packages that extend functionality of existing packages
# [centosplus]
# name=CentOS-$releasever - Plus - mirrors.aliyun.com
# failovermethod=priority
# baseurl=http://mirrors.aliyun.com/centos/$releasever/centosplus/$basearch/
#         http://mirrors.aliyuncs.com/centos/$releasever/centosplus/$basearch/
#         http://mirrors.cloud.aliyuncs.com/centos/$releasever/centosplus/$basearch/
# gpgcheck=1
# enabled=0
# gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7

# #contrib - packages by Centos Users
# [contrib]
# name=CentOS-$releasever - Contrib - mirrors.aliyun.com
# failovermethod=priority
# baseurl=http://mirrors.aliyun.com/centos/$releasever/contrib/$basearch/
#         http://mirrors.aliyuncs.com/centos/$releasever/contrib/$basearch/
#         http://mirrors.cloud.aliyuncs.com/centos/$releasever/contrib/$basearch/
# gpgcheck=1
# enabled=0
# gpgkey=http://mirrors.aliyun.com/centos/RPM-GPG-KEY-CentOS-7
# EOF
	elif [ -f /etc/arch-release ];
	then
		echo 'Arch系统'
		sudo pacman -Sy zsh tmux autojump git vim tree cmake make g++ 
		echo '更新镜像排名'
		sudo pacman-mirrors -c China
		echo '更新数据源'
		sudo pacman -Syy
		sudo pacman -S archlinuxcn-keyring
		echo '配置pacman 清华源'
		cat << EOF >> /etc/pacman.conf
[archlinuxcn]
SigLevel = Optional TrustedOnly
Server = https://mirrors.tuna.tsinghua.edu.cn/archlinuxcn/$arch
EOF

	else
		echo '未知操作系统'
	fi
}

getos

# if [[ ! -f /usr/bin/git || ! -f /usr/bin/tmux || ! -f /usr/bin/vim || ! -f /usr/bin/autojump]]; 
if [ ! -f /usr/bin/git ]; 
then 
	echo '请先安装git tmux vim autojump工具'
	exit 1
else
	echo '设置记住密码（默认15分钟）'
	echo 'git config --global credential.helper cache'
	echo '如果想自己设置时间'
	echo "git config credential.helper 'cache --timeout=3600'"
	echo '长期存储密码 增加远程地址的时候带上密码也是可以的'
	echo 'git config --global credential.helper store'
fi

if [ -d $HOME/.oh-my-zsh ]; 
then 
	echo 'oh-my-zsh已经安装';
else 
	if [ ! -f '/usr/bin/zsh' ]; then
		echo '未安装zsh,请自行安装！'
		exit 1
	fi

	echo '安装oh-my-zsh'
	sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
	echo '下载插件'
	git clone https://github.com/zsh-users/zsh-autosuggestions $ZSH_CUSTOM/plugins/zsh-autosuggestions
	git clone https://github.com/zsh-users/zsh-syntax-highlighting $ZSH_CUSTOM/plugins/zsh-syntax-highlighting
	git clone "https://github.com/MichaelAquilina/zsh-autoswitch-virtualenv.git" "$ZSH_CUSTOM/plugins/autoswitch_virtualenv"
	git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
	~/.fzf/install
	echo '# 配置项自行加入~/.zshrc文件
plugins=(
	git
	zsh-autosuggestions
	zsh-syntax-highlighting
	autojump
	autoswitch_virtualenv
	fzf
)
bindkey ',' autosuggest-accept
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
alias apt-get="sudo apt-get"
alias k="kubectl"
alias cdd="cd"'
fi

if [ ! -d /opt/go ];
then
	echo '安装golang环境 1.16.6'
	wget https://studygolang.com/dl/golang/go1.16.6.linux-amd64.tar.gz
	sudo tar -zxvf go1.16.6.linux-amd64.tar.gz -C /opt
	sudo mkdir -p /opt/gopath
	cat << EOF >> ~/.zshrc
export GOROOT=/opt/go
export GOBIN=/opt/gopath/bin
export GOPATH=/opt/gopath
export GOPROXY=https://goproxy.io,direct
export PATH=$PATH:/opt/go/bin:/opt/gopath/bin
EOF
	source ~/.zshrc
	echo '安装golang工具库'
	go get github.com/lflxp/showme
	go get -u gitee.com/lflxp/fhst
	go get -u github.com/silenceper/gowatch
	go get github.com/spf13/cobra/cobra
	go get golang.org/x/tools/cmd/stringer
	go get github.com/smartystreets/goconvey
else
	echo '已安装golang'
	go version
fi

if [ -d $HOME/.cargo ];
then
	echo '已安装rust，跳过'
else
	echo '安装rust'
	curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh
	rustup install nightly
	rustup default nightly
	echo '加速rust'
	cat << EOF > ~/.cargo/config
[source.crates-io]
registry = "https://github.com/rust-lang/crates.io-index"
replace-with = 'ustc'
[source.ustc]
registry = "git://mirrors.ustc.edu.cn/crates.io-index"
EOF
fi

if [ -f /usr/bin/vim ];
then
	echo '已安装vim'
else
	echo '安装vim'
	if [ ! -f /usr/bin/vim ]; 
	then 
		echo '请自行安装vim'
	else 
		echo '进行vim插件安装'
		git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
		cat << EOF > ~/.vimrc
set nocompatible
filetype on "开启文件类型侦测
filetype indent on "适应不同语言的智能缩进
syntax enable "开启语法高亮功能
syntax on "允许用定制语法高亮配色方案替换默认方案
set number "显示行号   
set cursorline
set ruler " 打开状态栏标尺  
set laststatus=2
set tabstop=4   "tab 代表4个空格
set softtabstop=4       "使用tab时 tab空格数   
set shiftwidth=4   " 默认缩进4个空格    
set expandtab      " 使用空格替换tab
set showmatch           "高亮显示匹配的括号
set hlsearch            "高亮显示搜索结果
set wildmenu "vim命令自动补全
set rtp+=~/.vim/bundle/Vundle.vim 
call vundle#begin()                                                      
Plugin 'VundleVim/Vundle.vim'      
Plugin 'fatih/vim-go' " golang语言插件
Plugin 'scrooloose/nerdtree' " 树形目录插件
Plugin 'vim-airline/vim-airline'  " 信息栏美化插件
Plugin 'vim-airline/vim-airline-themes'
Plugin 'scrooloose/nerdcommenter' " 多行注释
Plugin 'jiangmiao/auto-pairs' "括号、引号自动补全
Plugin 'Yggdroot/indentLine' " 显示缩进
Plugin 'racer-rust/vim-racer'          "  rust插件列表1
Plugin 'rust-lang/rust.vim'            "  rust插件列表2
Plugin 'kien/ctrlp.vim' " 超级搜索插件
Plugin 'jistr/vim-nerdtree-tabs'
Plugin 'Xuyuanp/nerdtree-git-plugin' " 帮助显示一些 git 信息
" Plugin 'vim-airline/fonts'
Plugin 'Valloric/YouCompleteMe'  " 自动补全插件 
call vundle#end()   
filetype plugin indent on   
let mapleader=";"   
map <leader>n :NERDTreeToggle<CR>  
let g:go_fmt_command = "goimports"  
let g:go_highlight_functions = 1  
let g:go_highlight_methods = 1  
let g:go_highlight_structs = 1  
let g:ycm_add_preview_to_completeopt = 0   
let g:ycm_min_num_of_chars_for_completion = 1     
let g:ycm_auto_trigger = 1   
let mapleader=","
let g:indentLine_char='┆' "缩进指示线符
let g:indentLine_enabled = 1 "开启缩进指示
let g:rustfmt_autosave = 1
set completeopt-=preview
EOF
	fi
fi
